#include <avr/io.h>
#include <avr/interrupt.h>
#include <lcd.h>
#include <adc.h>
#include <stdio.h>
#include <util/delay.h>


#define CLOCK_DIV 0x02


void main_loop();

volatile int count = 0;
int pwm_thresh = 20;

int main(void)
{
	lcd_init();
	adc_init(5);
	/* Set up Port B; input on Bit 0 and 1*/
	DDRB &= ~0x01;
	DDRB &= ~0x02;
	DDRB |=  0x04;
	cli();

	/*Timer*/
	//set prescaler to 1, and enable clock
	TCCR1B = CLOCK_DIV<<CS10|1<<ICNC1|1<<ICES1;
	TCCR1A = 0x00;
	//with this setting, the total time per millis is 20,000 ticks

	//enable interrupt 
	TIMSK1 = 1<<ICIE1;


	/*Timer for PWM*/
	//set pin3 of port D to output
	DDRD |= 0x08;
	//set clock division and mode to /1 and PWM
	//TCCR2A = 0x83;
	TCCR2A = ((1<<COM2B1)|(1<<COM2A1)|(1<<WGM20)|(1<<WGM21));
	//TCCR2B = 0x09;
	TCCR2B = ((1<<WGM22)|(CLOCK_DIV<<CS20));

	//no interrupt
	TIMSK2 = 0x00;

	//initialise the comparison register
	OCR2A = 255;
	OCR2B = pwm_thresh;
	/* Globally enable interrupts */
	sei();

	main_loop();

	return 0;
}


void main_loop()
{
	char buffer[30];
	int duty;
	//main program loop
	while (1)
	{
		duty = adc_read()/3;
		lcd_clear();
		sprintf(buffer, "COUNT:%03d\nDUTY:%03d",count, duty);	
		lcd_display(buffer);
		count = 0;
		OCR2B = duty;
		_delay_ms(100);
		//pwm_thresh = (pwm_thresh+10)%255;
		//OCR2B = pwm_thresh;
	}

}


volatile int time;
ISR(TIMER1_CAPT_vect)
{
	int neg = 1;
	if(PINB&0x02)
		neg = -1;
	//get the count since last interrupt
	uint32_t time = (uint32_t)TCNT1;

	count = (int)(421052/time);
	count = count*neg;
	//reset the count since last interrupt
	TCNT1 = 0;	
}
